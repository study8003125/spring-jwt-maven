package com.example.springjwtmaven;

import com.example.springjwtmaven.config.RsaKeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(RsaKeyProperties.class)
@SpringBootApplication
public class SpringJwtMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJwtMavenApplication.class, args);
	}

}
